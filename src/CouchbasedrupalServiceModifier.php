<?php

namespace Drupal\couchbasedrupal;

use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

class CouchbasedrupalServiceModifier implements ServiceModifierInterface {
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
  }
}
