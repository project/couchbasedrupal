<?php

namespace Drupal\couchbasedrupal;

class TranscoderPassThru implements TranscoderInterface {

  /**
   * Default passthru encoder which simply passes data
   * as-is rather than performing any transcoding.
   *
   * @internal
   */
  function encode($value) {
    return array($value, 0, 0);
  }

  /**
   * Default passthru encoder which simply passes data
   * as-is rather than performing any transcoding.
   *
   * @internal
   */
  function decode($bytes, $flags, $datatype) {
    return $bytes;
  }

}
