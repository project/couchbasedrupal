<?php

namespace Drupal\couchbasedrupal\Cache;

use \Drupal\couchbasedrupal\TranscoderInterface;

/**
 * Optimized transcoder for speed. Because we are using
 * couchbase only as a key value store, the data needs
 * not to be stored as a json document. Faster to serialize
 * and unserialize.
 * 
 * This is just like the default transcoder with
 * custom options.
 * 
 */
class CouchbaseCacheTranscoder implements TranscoderInterface {

  /**
   * Encoder options.
   * 
   * @var array
   */
  protected $options;

  public function __construct() {
    $this->options = [
        'sertype' => function_exists('igbinary_serialize') ? COUCHBASE_SERTYPE_IGBINARY : COUCHBASE_SERTYPE_PHP,
        'cmprtype' => COUCHBASE_CMPRTYPE_NONE,
        'cmprthresh' => 0,
        'cmprfactor' => 0
    ];
  }

  function encode($value) {
    return couchbase_basic_encoder_v1($value, $this->options);
  }

  function decode($bytes, $flags, $datatype) {
    return couchbase_basic_decoder_v1($bytes, $flags, $datatype, ['jsonassoc' => false]);
  }

}
