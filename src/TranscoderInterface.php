<?php

namespace Drupal\couchbasedrupal;

interface TranscoderInterface {

  /**
   * The value to encode.
   * 
   * This function must return a array in the form of
   * [$encoded, $flags, $datatype] where $flags
   * and $datatype are metadata that is sent to
   * decode() when trying to retrieve the data.
   * 
   * @param mixed $value 
   */
  function encode($value);

  /**
   * Function to decode the data.
   * 
   * @param mixed $bytes 
   * 
   * @param mixed $flags 
   * 
   * @param mixed $datatype 
   */
  function decode($bytes, $flags, $datatype);

}
