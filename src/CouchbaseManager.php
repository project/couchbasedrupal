<?php

namespace Drupal\couchbasedrupal;

use Drupal\Core\Site\Settings;

use Drupal\couchbasedrupal\CouchbaseBucket as Bucket;

/**
 * This is a service used to manage the connections
 * to the different couchbase servers.
 */
class CouchbaseManager {

  /**
   * Returns a 12 character length MD5.
   *
   * @param string $string
   * @return string
   */
  public static function shortMd5($string) {
    return substr(base_convert(md5($string), 16,32), 0, 12);
  }

  /**
   * Couchbase server settings.
   *
   * @var array
   */
  var $settings;

  /**
   * Prefix for this site.
   *
   * @var string
   */
  protected $site_prefix;

  /**
   * Get the prefix to use for this site.
   *
   * @return string
   */
  public function getSitePrefix() {
    return $this->site_prefix;
  }

  /**
   * Settings
   *
   * @param Settings $settings
   */
  public function __construct(Settings $settings, $root, $site_path) {
    // Add a default for a local couchbase server.
    $this->settings = ['servers' =>
      ['default' => ['uri' => 'couchbase://127.0.0.1'] ]
    ];
    // Merge or override with custom settings.
    $this->settings = array_merge($this->settings, $settings::get('couchbasedrupal', []));
    $this->site_prefix = static::shortMd5(Settings::getApcuPrefix('apc_backend', $root, $site_path));
  }

  /**
   * List of instanced clusters.
   *
   * This is static to ensure connections are reused. The SDK is supposed
   * to do this internally, but just to keep on the safe side.
   *
   * @var \CouchbaseCluster[]
   */
  protected static $clusters = [];

  /**
   * This is static to ensure bucket connections are reused.
   *
   * @var Bucket[]
   */
  protected static $buckets = [];


  /**
   * List of transcoders
   *
   * @var TranscoderInterface[]
   */
  protected $transcoders = [];

  /**
   * Gets the bucket name to use for a specific cluster. Defaults
   * to 'default' when this is not configured in settings.php
   *
   * @param string $name
   * @return string
   */
  public function getClusterDefaultBucketName($name = 'default') {
    if (isset($this->settings['servers'][$name]['bucket'])) {
      return $this->settings['servers'][$name]['bucket'];
    }
    return 'default';
  }

  /**
   * Gets the password used for SASL authentication
   * on the bucket.
   *
   * @param string $name
   * @return string|NULL
   */
  public function getClusterDefaultBucketPassword($name = 'default') {
    if (isset($this->settings['servers'][$name]['bucket_password'])) {
      return $this->settings['servers'][$name]['bucket_password'];
    }
    return NULL;
  }

  /**
   * Retrieve a connection to the couchbase server.
   *
   * @param string $name
   * @return bool|\CouchbaseCluster
   */
  public function getCluster($name = 'default') {

    if (isset(self::$clusters[$name])) {
      return self::$clusters[$name];
    }

    // For this service to work we need the CouchbaseCluster class.
    if (!class_exists(\CouchbaseCluster::class)) {
      return FALSE;
    }

    if (!isset($this->settings['servers'][$name]['uri'])) {
      return FALSE;
    }

    $uri = $this->settings['servers'][$name]['uri'];

    $server = $this->ParseUri($uri);

    $server_url = $server['scheme'] . '://' . $server['host'];

    if (isset($server['port']) && !empty($server['port'])) {
      $server_url .= ':' . $server['port'];
    }

    // There is a LOT of logic in the couchbase drupal driver
    // that depends on detailed error codes, so force them...
    $query = [];
    if (isset($server['query']) && !empty($server['query'])) {
      parse_str($server['query'], $query);
    }
    $query = array_merge($query, ['detailed_errcodes' => '1']);
    $server_url .= '?' . http_build_query($query);

    if (isset($server['username']) && isset($server['password'])) {
      self::$clusters[$name] = new \CouchbaseCluster($server_url, $server['username'], $server['password']);
    }
    else {
      self::$clusters[$name] = new \CouchbaseCluster($server_url);
    }

    return self::$clusters[$name];
  }

  /**
   * Get a list of all available
   * bucket names.
   */
  public function listServers() {
    return array_keys($this->settings['servers']);
  }

  /**
   * Get a bucket trough their configuration ID.
   *
   * @param string $id
   * @param string $transcoder
   * @return Bucket
   */
  public function getBucketFromConfig($id, $transcoder = NULL) {
    $bucket_name = $this->getClusterDefaultBucketName($id);
    $bucket_password = $this->getClusterDefaultBucketPassword($id);
    return $this->getBucket($id, $bucket_name, $bucket_password, $transcoder);
  }

  /**
   * Retrieve a couchbase bucket.
   *
   * @param string $cluster_name
   *   The name of the cluster.
   *
   * @param string $bucket_name
   *   The name of the bucket.
   *
   * @param string $transcoder
   *   The class used as a transcoder.
   *
   * @return Bucket
   */
  public function getBucket($cluster_name = 'default', $bucket_name = 'default', $password = NULL, $transcoder = NULL) {
    $cluster = $this->getCluster($cluster_name);
    // We will store instances of the bucket with
    // different transcoders.
    $key = implode('::', [$cluster_name, $bucket_name, $transcoder, $password]);
    if (!isset(self::$buckets[$key])) {
      if ($transcoder) {
        if (!isset($this->transcoders[$transcoder])) {
          $this->transcoders[$transcoder] = new $transcoder();
        }
        $transcoder = $this->transcoders[$transcoder];
      }
      self::$buckets[$key] = new Bucket($cluster, $bucket_name, $password, $transcoder);
    }
    return self::$buckets[$key];
  }

  /**
   * Parse a couchbase connection URI.
   *
   * @param string $uri
   */
  protected function ParseUri($url) {
    $info = parse_url($url);
    if (!isset($info['scheme'], $info['host'])) {
      throw new \InvalidArgumentException('Minimum requirement: driver://host');
    }
    $info += array(
      'user' => '',
      'pass' => '',
      'fragment' => '',
    );

    $database = [
      'scheme' => $info['scheme'],
      'username' => $info['user'],
      'password' => $info['pass'],
      'host' => $info['host'],
    ];

    if (isset($info['port'])) {
      $database['port'] = $info['port'];
    }

    if (isset($info['path'])) {
      $database['path'] = $info['path'];
    }

    if (isset($info['fragment'])) {
      $database['fragment'] = $info['fragment'];
    }

    if (isset($info['query'])) {
      $database['query'] = $info['query'];
    }

    return $database;
  }
}
