<?php

namespace Drupal\couchbasedrupal\Tests\Cache;

use Drupal\supercache\Tests\Generic\Cache\RawBackendGeneralTests as GeneralTests;

class RawBackendGeneralTests extends GeneralTests {
  use RawBackendGeneralTestCaseTrait;
}
