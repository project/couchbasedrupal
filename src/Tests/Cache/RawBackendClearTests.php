<?php

namespace Drupal\couchbasedrupal\Tests\Cache;

use Drupal\supercache\Tests\Generic\Cache\BackendClearTests as ClearTests;

class RawBackendClearTests extends ClearTests {
  use RawBackendGeneralTestCaseTrait;
}
