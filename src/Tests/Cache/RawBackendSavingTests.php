<?php

namespace Drupal\couchbasedrupal\Tests\Cache;

use Drupal\supercache\Tests\Generic\Cache\BackendSavingTests as SavingTests;

class RawBackendSavingTests extends SavingTests {
  use RawBackendGeneralTestCaseTrait;
}
