<?php

namespace Drupal\couchbasedrupal\Tests\Cache;

use Drupal\supercache\Tests\Generic\Cache\BackendClearTests as ClearTests;

class BackendClearTests extends ClearTests {
  use BackendGeneralTestCaseTrait;
}
