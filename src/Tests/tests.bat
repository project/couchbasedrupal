php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\BackendClearTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\BackendExpireTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\BackendGetMultipleTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\BackendSavingTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\RawBackendClearTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\RawBackendExpireTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\RawBackendGeneralTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\RawBackendGetMultipleTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\RawBackendSavingTests"
timeout 3
php ../../../../core/scripts/run-tests.sh --php "php" --class "Drupal\couchbasedrupal\Tests\Cache\TagsChecksumInvalidatorTests"