<?php

/**
 * Implements hook_requirements().
 */
function couchbasedrupal_requirements($phase) {
  $requirements = [];

  $extensionloaded = extension_loaded('couchbase');

  if ($phase == 'install' || $phase == 'runtime') {
    if (!$extensionloaded) {
      $requirements['couchbase_extension']['severity'] = REQUIREMENT_ERROR;
      $requirements['couchbase_extension']['title'] = t('Extension not available');
      $requirements['couchbase_extension']['value'] = t('The <a href="http://developer.couchbase.com/documentation/server/4.1/sdks/php-2.0/download-links.html">couchbase</a> extension must be installed in order to use the Couchbase integration.');
    }
  }

  if ($phase == 'runtime') {

    /** @var \Drupal\couchbasedrupal\CouchbaseManager */
    $manager = \Drupal::service('couchbasedrupal.manager');

    $summary = [];

    foreach ($manager->listServers() as $server) {
      $bucket_name = $manager->getClusterDefaultBucketName($server);
      $bucket_password = $manager->getClusterDefaultBucketPassword($server);
      $bucket = $manager->getBucket($server, $bucket_name, $bucket_password);
      /** @var \CouchbaseBucketManager */
      $m = $bucket->manager();
      $info = $m->info();
      $quota = round($info['basicStats']['quotaPercentUsed'], 1);
      $stats = $info['basicStats'];
      $diskUsed = isset($stats['diskUsed']) ? format_size($stats['diskUsed']) : 'missing';
      $dataUsed = isset($stats['dataUsed']) ? format_size($stats['dataUsed']) : 'missing';
      $memUsed = isset($stats['memUsed']) ? format_size($stats['memUsed']) : 'missing';
      $summary[] = "bucket: {$server} | quota usage: {$quota}% | items:  {$info['basicStats']['itemCount']} | disk: {$diskUsed} | data: {$dataUsed} | memory: {$memUsed} | type: {$info['bucketType']}";
    }

    $requirements['couchbase_usage'] = array(
      'title' => t('Couchbase storage'),
      'value' => '',
      'severity' => REQUIREMENT_OK,
      'description' => implode('<br/>', $summary)
    );
  }

  return $requirements;
}
